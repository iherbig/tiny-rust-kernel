#A Minimum x86 Kernel#

Following Philipp Oppermann's guide [here](http://os.phil-opp.com/multiboot-kernel.html). This kernel is incredibly simple. 4K stack, identity mapped pages, and interrupts are rather difficult to handle considering the fact that core Rust relies on floating point (and storing floating point registers for context switches is expensive). But it boots and you can print to screen using a VGA interface, which is neat.

I'm following along as Oppermann updates his blog and using it as a learning experience. It's tremendously cool watching your kernel boot and knowing you wrote all of the code that does it. If you're looking at this, I'd suggest you read through the blog and try it out yourself.